/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2023/9/17 23:41:08                           */
/*==============================================================*/


drop table if exists country_code;

drop table if exists detected_item;

drop table if exists instrument;

drop table if exists instrument_location;

drop table if exists instrument_model;

drop table if exists instrument_model_category;

drop table if exists instrument_model_detected_item;

drop table if exists instrument_model_file;

drop table if exists manufacturer_info;

/*==============================================================*/
/* Table: country_code                                          */
/*==============================================================*/
create table country_code
(
   manufacturer_country_code_id bigint not null auto_increment comment '厂家国别码ID',
   manufacturer_country_code_name varchar(100) not null comment '厂家国别码名称',
   primary key (manufacturer_country_code_id)
);

/*==============================================================*/
/* Table: detected_item                                         */
/*==============================================================*/
create table detected_item
(
   detected_item_id     bigint not null auto_increment comment '检测项目ID',
   detected_item_name   varchar(100) not null comment '检测项目名称',
   primary key (detected_item_id)
);

/*==============================================================*/
/* Table: instrument                                            */
/*==============================================================*/
create table instrument
(
   instrument_id        bigint not null auto_increment comment '仪器ID',
   instrument_model_id  bigint not null comment '仪器型号ID',
   instrument_name      varchar(100) not null comment '仪器名称',
   instrument_location_id bigint not null comment '仪器位置ID',
   buy_datetime         datetime not null comment '购入时间',
   net_datetime         datetime not null comment '入网时间',
   instrument_status    tinyint not null comment '仪器状态',
   user_id              bigint not null comment '用户ID',
   remark               varchar(255) comment '备注',
   primary key (instrument_id)
);

/*==============================================================*/
/* Table: instrument_location                                   */
/*==============================================================*/
create table instrument_location
(
   instrument_location_id bigint not null auto_increment comment '仪器位置ID',
   location_name        varchar(100) not null comment '位置名称',
   longitude            float(9) not null comment '经度',
   latitude             float(9) not null comment '纬度',
   remark               varchar(255) comment '备注',
   primary key (instrument_location_id)
);

/*==============================================================*/
/* Table: instrument_model                                      */
/*==============================================================*/
create table instrument_model
(
   instrument_model_id  bigint not null auto_increment comment '仪器型号ID',
   instrument_model_category_id bigint not null comment '仪器型号类别ID',
   instrument_model_name varchar(100) not null comment '仪器型号名称',
   main_function        varchar(255) not null comment '主要功能',
   tech_param           varchar(255) not null comment '技术参数',
   isTrain              tinyint not null comment '是否需要培训',
   remark               varchar(255) comment '备注',
   primary key (instrument_model_id)
);

/*==============================================================*/
/* Table: instrument_model_category                             */
/*==============================================================*/
create table instrument_model_category
(
   instrument_model_category_id bigint not null auto_increment comment '仪器型号类别ID',
   large_category_name  varchar(40) not null comment '大类名称',
   medium_category_name varchar(40) not null comment '中类名称',
   small_category_name  varchar(40) not null comment '小类名称',
   large_category_code  varchar(40) not null comment '大类编码',
   medium_category_code varchar(40) not null comment '中类编码',
   small_category_code  varchar(40) not null comment '小类编码',
   remark               varchar(255) comment '备注',
   primary key (instrument_model_category_id)
);

/*==============================================================*/
/* Table: instrument_model_detected_item                        */
/*==============================================================*/
create table instrument_model_detected_item
(
   detected_item_id     bigint not null auto_increment comment '检测项目ID',
   instrument_model_id  bigint not null comment '仪器型号ID',
   primary key (detected_item_id)
);

/*==============================================================*/
/* Table: instrument_model_file                                 */
/*==============================================================*/
create table instrument_model_file
(
   instrument_model_file_id bigint not null auto_increment comment '仪器型号文件ID',
   instrument_model_id  bigint not null comment '仪器型号ID',
   file_type            tinyint not null comment '文件类型',
   file_address         varchar(255) not null comment '文件地址',
   primary key (instrument_model_file_id)
);

/*==============================================================*/
/* Table: manufacturer_info                                     */
/*==============================================================*/
create table manufacturer_info
(
   manufacturer_id      bigint not null auto_increment comment '厂家ID',
   instrument_model_id  bigint not null comment '仪器型号ID',
   manufacturer_name    varchar(100) not null comment '厂家名称',
   manufacturer_country_code_id bigint not null comment '厂家国别码ID',
   contact              varchar(40) comment '联系人',
   tele                 varchar(11) comment '电话',
   primary key (manufacturer_id)
);

alter table instrument add constraint FK_Reference_1 foreign key (instrument_model_id)
      references instrument_model (instrument_model_id) on delete restrict on update restrict;

alter table instrument add constraint FK_Reference_3 foreign key (instrument_location_id)
      references instrument_location (instrument_location_id) on delete restrict on update restrict;

alter table instrument_model add constraint FK_Reference_2 foreign key (instrument_model_category_id)
      references instrument_model_category (instrument_model_category_id) on delete restrict on update restrict;

alter table instrument_model_detected_item add constraint FK_Reference_7 foreign key (instrument_model_id)
      references instrument_model (instrument_model_id) on delete restrict on update restrict;

alter table instrument_model_detected_item add constraint FK_Reference_8 foreign key (detected_item_id)
      references detected_item (detected_item_id) on delete restrict on update restrict;

alter table instrument_model_file add constraint FK_Reference_6 foreign key (instrument_model_id)
      references instrument_model (instrument_model_id) on delete restrict on update restrict;

alter table manufacturer_info add constraint FK_Reference_4 foreign key (instrument_model_id)
      references instrument_model (instrument_model_id) on delete restrict on update restrict;

alter table manufacturer_info add constraint FK_Reference_5 foreign key (manufacturer_country_code_id)
      references country_code (manufacturer_country_code_id) on delete restrict on update restrict;

