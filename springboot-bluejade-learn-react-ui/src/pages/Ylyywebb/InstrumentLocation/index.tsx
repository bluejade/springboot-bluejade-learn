import React, { useState, useRef } from 'react';
import type { FormInstance } from 'antd';
import { Button, message, Modal } from 'antd';
import { ActionType, PageContainer, ProColumns, ProTable } from '@ant-design/pro-components';
import { PlusOutlined, DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { getInstrumentLocationList, removeInstrumentLocation, addInstrumentLocation, updateInstrumentLocation, exportInstrumentLocation } from './service';

import { InstrumentLocation,InstrumentLocationListParams } from "./data";

import UpdateForm from './edit';

const handleAdd = async (fields: InstrumentLocation) => {
    const hide = message.loading('正在添加');
    try {
        const resp = await addInstrumentLocation({ ...fields });
        hide();
        if (resp.code === 200) {
            message.success('添加成功');
        } else {
            message.error(resp.msg);
        }
        return true;
    } catch (error) {
        hide();
        message.error('添加失败请重试！');
        return false;
    }
};

const handleUpdate = async (fields: InstrumentLocation) => {
    const hide = message.loading('正在更新');
    try {
        const resp = await updateInstrumentLocation(fields);
        hide();
        if (resp.code === 200) {
            message.success('更新成功');
        } else {
            message.error(resp.msg);
        }
        return true;
    } catch (error) {
        hide();
        message.error('配置失败请重试！');
        return false;
    }
};

const handleRemoveOne = async (selectedRow: InstrumentLocation) => {
    const hide = message.loading('正在删除');
    if (!selectedRow) return true;
    try {
        const params = [selectedRow.instrumentLocationId];
        const resp = await removeInstrumentLocation(params.join(','));
        hide();
        if (resp.code === 200) {
            message.success('删除成功，即将刷新');
        } else {
            message.error(resp.msg);
        }
        return true;
    } catch (error) {
        hide();
        message.error('删除失败，请重试');
        return false;
    }
};

const handleRemove = async (selectedRows: InstrumentLocation[]) => {
    const hide = message.loading('正在删除');
    if (!selectedRows) return true;
    try {
        const resp = await removeInstrumentLocation(selectedRows.map((row) => row.instrumentLocationId).join(','));
        hide();
        if (resp.code === 200) {
            message.success('删除成功，即将刷新');
        } else {
            message.error(resp.msg);
        }
        return true;
    } catch (error) {
        hide();
        message.error('删除失败，请重试');
        return false;
    }
};

const handleExport = async () => {
    const hide = message.loading('正在导出');
    try {
        await exportInstrumentLocation();
        hide();
        message.success('导出成功');
        return true;
    } catch (error) {
        hide();
        message.error('导出失败，请重试');
        return false;
    }
};

const InstrumentLocationTableList: React.FC = () => {
    const formTableRef = useRef<FormInstance>();

    const [modalVisible, setModalVisible] = useState<boolean>(false);

    const actionRef = useRef<ActionType>();
    const [currentRow, setCurrentRow] = useState<InstrumentLocation>();
    const [selectedRows, setSelectedRows] = useState<InstrumentLocation[]>([]);

    const columns: ProColumns<InstrumentLocation>[] = [
        {
            title: "仪器位置ID",
            dataIndex: 'instrumentLocationId',
            valueType: 'text',
            hideInSearch: true,
        },
        {
            title: "位置名称",
            dataIndex: 'locationName',
            valueType: 'text',
        },
        {
            title: "经度",
            dataIndex: 'longitude',
            valueType: 'text',
        },
        {
            title: "纬度",
            dataIndex: 'latitude',
            valueType: 'text',
        },
        {
            title: "操作",
            dataIndex: 'option',
            width: '120px',
            valueType: 'option',
            render: (_, record) => [
                <Button
                    type="link"
                    size="small"
                    key="edit"
                    onClick={() => {
                        setModalVisible(true);
                        setCurrentRow(record);
                    }}
                >
                    编辑
                </Button>,
                <Button
                    type="link"
                    size="small"
                    danger
                    key="batchRemove"
                    onClick={async () => {
                        Modal.confirm({
                            title: '删除',
                            content: '确定删除该项吗？',
                            okText: '确认',
                            cancelText: '取消',
                            onOk: async () => {
                                const success = await handleRemoveOne(record);
                                if (success) {
                                    if (actionRef.current) {
                                        actionRef.current.reload();
                                    }
                                }
                            },
                        });
                    }}
                >
                    删除
                </Button>,
            ],
        },
    ];

    return (
        <PageContainer>
            <div style={{ width: '100%', float: 'right' }}>
                <ProTable<InstrumentLocation>
                    headerTitle={"查询表格"}
                    actionRef={actionRef}
                    formRef={formTableRef}
                    rowKey="instrumentLocationId"
                    key="instrumentLocationList"
                    search={{
                        labelWidth: 120,
                    }}
                    toolBarRender={() => [
                        <Button
                            type="primary"
                            key="add"
                            onClick={async () => {
                                setCurrentRow(undefined);
                                setModalVisible(true);
                            }}
                        >
                            <PlusOutlined /> 新建
                        </Button>,
                        <Button
                            type="primary"
                            key="remove"
                            hidden={selectedRows?.length === 0}
                            onClick={async () => {
                                Modal.confirm({
                                    title: '是否确认删除所选数据项?',
                                    icon: <ExclamationCircleOutlined />,
                                    content: '请谨慎操作',
                                    async onOk() {
                                        const success = await handleRemove(selectedRows);
                                        if (success) {
                                            setSelectedRows([]);
                                            actionRef.current?.reloadAndRest?.();
                                        }
                                    },
                                    onCancel() {},
                                });
                            }}
                        >
                            <DeleteOutlined />删除
                        </Button>,
                        <Button
                            type="primary"
                            key="export"
                            onClick={async () => {
                                handleExport();
                            }}
                        >
                            <PlusOutlined />导出
                        </Button>,
                    ]}
                    request={(params) =>
                        getInstrumentLocationList({ ...params } as InstrumentLocationListParams).then((res) => {
                            const result = {
                                data: res.rows,
                                total: res.total,
                                success: true,
                            };
                            return result;
                        })
                    }
                    columns={columns}
                    rowSelection={{
                        onChange: (_, selectedRows) => {
                            setSelectedRows(selectedRows);
                        },
                    }}
                />
            </div>
            <UpdateForm
                onSubmit={async (values) => {
                    let success = false;
                    if (values.instrumentLocationId) {
                        success = await handleUpdate({ ...values } as InstrumentLocation);
                    } else {
                        success = await handleAdd({ ...values } as InstrumentLocation);
                    }
                    if (success) {
                        setModalVisible(false);
                        setCurrentRow(undefined);
                        if (actionRef.current) {
                            actionRef.current.reload();
                        }
                    }
                }}
                onCancel={() => {
                    setModalVisible(false);
                    setCurrentRow(undefined);
                }}
                open={modalVisible}
                values={currentRow || {}}
            />
        </PageContainer>
        );
};
export default InstrumentLocationTableList;