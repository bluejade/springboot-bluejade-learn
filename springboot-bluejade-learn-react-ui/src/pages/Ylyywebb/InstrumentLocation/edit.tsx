import React, { useEffect } from 'react';
import {
    ProForm,
    ProFormDigit,
    ProFormText,
} from '@ant-design/pro-components';
import { Form, Modal} from 'antd';

import { InstrumentLocation } from "./data";

export type InstrumentLocationFormData = Record<string, unknown> & Partial<InstrumentLocation>;

export type InstrumentLocationFormProps = {
    onCancel: (flag?: boolean, formVals?: InstrumentLocationFormData) => void;
    onSubmit: (values: InstrumentLocationFormData) => Promise<void>;
    open: boolean;
    values: Partial<InstrumentLocation>;
};

const InstrumentLocationForm: React.FC<InstrumentLocationFormProps> = (props) => {
    const [form] = Form.useForm();

    useEffect(() => {
        form.resetFields();
        form.setFieldsValue({
            instrumentLocationId: props.values.instrumentLocationId,
            locationName: props.values.locationName,
            longitude: props.values.longitude,
            latitude: props.values.latitude,
        });
    }, [form, props]);

    const handleOk = () => {
        form.submit();
    };
    const handleCancel = () => {
        props.onCancel();
    };
    const handleFinish = async (values: Record<string, any>) => {
        props.onSubmit(values as InstrumentLocationFormData);
    };

    return (
        <Modal
            width={640}
            title={"编辑仪器位置表"}
            forceRender
            open={props.open}
            destroyOnClose
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <ProForm
                form={form}
                grid={true}
                submitter={false}
                layout="horizontal"
                onFinish={handleFinish}
            >
                <ProFormDigit
                    name="instrumentLocationId"
                    label={"编号"}
                    colProps={{ md: 12, xl: 24 }}
                    disabled
                    hidden={true}
                />
                <ProFormText
                    name="locationName"
                    label={"位置名称"}
                    placeholder="请输入位置名称"
                    rules={[
                        {
                            required: true,
                            message: "请输入位置名称",
                        },
                    ]}
                />
                <ProFormText
                    name="longitude"
                    label={"经度"}
                    placeholder="请输入经度"
                    rules={[
                        {
                            required: true,
                            message: "请输入经度",
                        },
                    ]}
                />
                <ProFormText
                    name="latitude"
                    label={"纬度"}
                    placeholder="请输入纬度"
                    rules={[
                        {
                            required: true,
                            message: "请输入纬度",
                        },
                    ]}
                />
            </ProForm>
        </Modal>
    );
};

export default InstrumentLocationForm;