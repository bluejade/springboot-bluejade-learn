export type InstrumentLocation = {
    instrumentLocationId: string;
    locationName: string;
    longitude: string;
    latitude: string;
}

export type InstrumentLocationListParams = {
    instrumentLocationId?: string;
    locationName?: string;
    longitude?: string;
    latitude?: string;
    pageSize?: string;
    current?: string;
}

export type InstrumentLocationInfoResult = {
    code: number;
    msg: string;
    data: InstrumentLocation;
}

export type InstrumentLocationPageResult = {
    code: number;
    msg: string;
    total: number;
    rows: Array<InstrumentLocation>;
}