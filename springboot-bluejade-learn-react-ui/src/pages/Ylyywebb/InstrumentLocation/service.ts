import { request } from '@umijs/max';

import { InstrumentLocation,InstrumentLocationListParams,InstrumentLocationPageResult, InstrumentLocationInfoResult } from "./data";

export async function getInstrumentLocationList(params?: InstrumentLocationListParams) {
    return request<InstrumentLocationPageResult>('/api/ylyywebb/instrumentLocation/list', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        params
    });
}

export function getInstrumentLocation(id: number) {
    return request<InstrumentLocationInfoResult>(`/api/ylyywebb/instrumentLocation/${id}`, {
        method: 'GET'
    });
}

export async function addInstrumentLocation(params: InstrumentLocation) {
    return request<API.Result>('/api/ylyywebb/instrumentLocation', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        data: params
    });
}

export async function updateInstrumentLocation(params: InstrumentLocation) {
    return request<API.Result>('/api/ylyywebb/instrumentLocation', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        data: params
    });
}

export async function removeInstrumentLocation(ids: string) {
    return request<API.Result>(`/api/ylyywebb/instrumentLocation/${ids}`, {
        method: 'DELETE'
    });
}

export function exportInstrumentLocation(params?: InstrumentLocationListParams) {
    return request<API.Result>(`/api/ylyywebb/instrumentLocation/export`, {
        method: 'GET',
        params
    });
}