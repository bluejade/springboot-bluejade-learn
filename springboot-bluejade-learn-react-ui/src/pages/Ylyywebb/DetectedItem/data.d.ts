export type DetectedItem = {
    detectedItemId: string;
    detectedItemName: string;
}

export type DetectedItemListParams = {
    detectedItemId?: string;
    detectedItemName?: string;
    pageSize?: string;
    current?: string;
}

export type DetectedItemInfoResult = {
    code: number;
    msg: string;
    data: DetectedItem;
}

export type DetectedItemPageResult = {
    code: number;
    msg: string;
    total: number;
    rows: Array<DetectedItem>;
}