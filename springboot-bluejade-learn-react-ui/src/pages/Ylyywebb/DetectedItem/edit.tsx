import React, { useEffect } from 'react';
import {
    ProForm,
    ProFormDigit,
    ProFormText,
} from '@ant-design/pro-components';
import { Form, Modal} from 'antd';

import { DetectedItem } from "./data";

export type DetectedItemFormData = Record<string, unknown> & Partial<DetectedItem>;

export type DetectedItemFormProps = {
    onCancel: (flag?: boolean, formVals?: DetectedItemFormData) => void;
    onSubmit: (values: DetectedItemFormData) => Promise<void>;
    open: boolean;
    values: Partial<DetectedItem>;
};

const DetectedItemForm: React.FC<DetectedItemFormProps> = (props) => {
    const [form] = Form.useForm();

    useEffect(() => {
        form.resetFields();
        form.setFieldsValue({
            detectedItemId: props.values.detectedItemId,
            detectedItemName: props.values.detectedItemName,
        });
    }, [form, props]);

    const handleOk = () => {
        form.submit();
    };
    const handleCancel = () => {
        props.onCancel();
    };
    const handleFinish = async (values: Record<string, any>) => {
        props.onSubmit(values as DetectedItemFormData);
    };

    return (
        <Modal
            width={640}
            title={"编辑检测项目表"}
            forceRender
            open={props.open}
            destroyOnClose
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <ProForm
                form={form}
                grid={true}
                submitter={false}
                layout="horizontal"
                onFinish={handleFinish}
            >
                <ProFormDigit
                    name="detectedItemId"
                    label={"编号"}
                    colProps={{ md: 12, xl: 24 }}
                    disabled
                    hidden={false}
                />
                <ProFormText
                    name="detectedItemName"
                    label={"检测项目名称"}
                    placeholder="请输入检测项目名称"
                    rules={[
                        {
                            required: true,
                            message: "请输入检测项目名称",
                        },
                    ]}
                />
            </ProForm>
        </Modal>
    );
};

export default DetectedItemForm;