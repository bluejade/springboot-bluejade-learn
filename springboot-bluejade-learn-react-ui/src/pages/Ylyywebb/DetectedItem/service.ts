import { request } from '@umijs/max';

import { DetectedItem,DetectedItemListParams,DetectedItemPageResult, DetectedItemInfoResult } from "./data";

export async function getDetectedItemList(params?: DetectedItemListParams) {
    return request<DetectedItemPageResult>('/api/ylyywebb/detectedItem/list', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        params
    });
}

export function getDetectedItem(id: number) {
    return request<DetectedItemInfoResult>(`/api/ylyywebb/detectedItem/${id}`, {
        method: 'GET'
    });
}

export async function addDetectedItem(params: DetectedItem) {
    return request<API.Result>('/api/ylyywebb/detectedItem', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        data: params
    });
}

export async function updateDetectedItem(params: DetectedItem) {
    return request<API.Result>('/api/ylyywebb/detectedItem', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        data: params
    });
}

export async function removeDetectedItem(ids: string) {
    return request<API.Result>(`/api/ylyywebb/detectedItem/${ids}`, {
        method: 'DELETE'
    });
}

export function exportDetectedItem(params?: DetectedItemListParams) {
    return request<API.Result>(`/api/ylyywebb/detectedItem/export`, {
        method: 'GET',
        params
    });
}