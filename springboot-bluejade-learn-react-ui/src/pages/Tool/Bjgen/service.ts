import { request } from "@umijs/max";
import { BjgenTableColumnListParams, BjgenTableListParams, BjgenTableType } from "./data";

// 查询所有表
export async function getBjgenTableList(params?: BjgenTableListParams) {
    const queryString = new URLSearchParams(params).toString();
    return request(`/api/bjcode/bjgen/table/list?${queryString}`, {
        data: params,
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
    });
}

//   根据表名查询该表的所有字段
export async function getBjgenTableColumnList(params?: BjgenTableColumnListParams) {
    const queryString = new URLSearchParams(params).toString();
    return request(`/api/bjcode/bjgen/tableColumn/ColumnList?${queryString}`, {
        data: params,
        method: 'get',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
    });
}

// 生成的代码的预览
export async function previewCode(tableName: any) {
    // eslint-disable-next-line no-param-reassign
    // tableName = "country_code";
    return request(`/api/bjcode/bjgen/tableColumn/preview/${tableName}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
    });
}

// 获取生成代码的基本信息数据
export async function getBjgenInfo(tableName: any) {
    // return request('/api/bjcode/bjgen/table/${tableName}', {
    return request(`/api/bjcode/bjgen/table/tableName/${tableName}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    },
    });
  }

// 添加生成代码的基本信息数据
export async function addBjgenInfo(params: BjgenTableType) {
    return request('/api/bjcode/bjgen/table', {
      method: 'POST',
      data: {
        ...params,
      },
    });
  }
  
  // 更新生成代码的基本信息数据
  export async function updateBjgenInfo(params: BjgenTableType) {
    return request('/api/bjcode/bjgen/table', {
      method: 'PUT',
      data: {
        ...params,
      },
    });
  }