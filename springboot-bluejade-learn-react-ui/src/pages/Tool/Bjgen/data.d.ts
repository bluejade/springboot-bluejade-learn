export type BjgenTableType = {
    createBy?: string,
    createTime?: string,
    updateBy?: string,
    updateTime?: string,
    remark?: string,
    tableId?: string,
    tableName?: string,
    tableComment?: string,
    className?: string,
    tplCategory?: string,
    columns?: any,

    packageName?: string,
    moduleName?: string,
    businessName?: string,
    functionName?: string,
};

export type BjgenTableListParams = {
    createBy?: string,
    createTime?: string,
    updateBy?: string,
    updateTime?: string,
    remark?: string,
    tableId?: string,
    tableName?: string,
    tableComment?: string,
    className?: string,
    tplCategory?: string,
    columns?: any
};

export type BjgenTableColumnListParams = {
    createBy?: string,
    createTime?: string,
    updateBy?: string,
    updateTime?: string,
    remark?: string,
    columnId?: any,
    tableId?: string,
    tableName?: string,
    columnName?: string,
    columnType?: string,
    columnComment?: string,
    isPrimaryKey?: string,
    isRequired?: string
};