import { ActionType, ProColumns, ProTable } from "@ant-design/pro-components";
import { useAccess } from "@umijs/max";
import { Button, Card, Layout, message } from "antd";
// import { Content } from "antd/es/layout/layout";
import { useRef, useState } from "react";
import { BjgenTableListParams, BjgenTableType } from "./data";
import type { FormInstance } from 'antd';
import PreviewForm from './components/PreviewCode';
import EditForm from './components/EditForm';

import { getBjgenInfo, getBjgenTableList, previewCode, addBjgenInfo, updateBjgenInfo } from './service';

const { Content } = Layout;

/**
 * 添加节点
 *
 * @param fields
 */
const handleAdd = async (fields: BjgenTableType) => {
    const hide = message.loading('正在添加');
    try {
        const resp = await addBjgenInfo({ ...fields });
        hide();
        if (resp.code === 200) {
            message.success('添加成功');
        } else {
            message.error(resp.msg);
        }
        return true;
    } catch (error) {
        hide();
        message.error('添加失败请重试！');
        return false;
    }
};

/**
 * 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: BjgenTableType) => {
    const hide = message.loading('正在更新');
    try {
        const resp = await updateBjgenInfo(fields);
        hide();
        if (resp.code === 200) {
            message.success('更新成功');
        } else {
            message.error(resp.msg);
        }
        return true;
    } catch (error) {
        hide();
        message.error('配置失败请重试！');
        return false;
    }
};

const BjgenCodeView: React.FC = () => {

    const [showPreview, setShowPreview] = useState<boolean>(false);
    const [preivewData, setPreivewData] = useState<boolean>(false);

    const [editModalVisible, setEditModalVisible] = useState<boolean>(false);
    const [currentRow, setCurrentRow] = useState<BjgenTableType>();
    const [bjgenInfo, setBjgenInfo] = useState<any>({});

    const actionRef = useRef<ActionType>();
    const formTableRef = useRef<FormInstance>();

    const access = useAccess();

    const columns: ProColumns<BjgenTableType>[] = [
        {
            title: '表名',
            dataIndex: 'tableName',
            valueType: 'textarea',
        },
        {
            title: '表描述',
            dataIndex: 'tableComment',
            valueType: 'textarea',
        },
        {
            title: '操作',
            dataIndex: 'option',
            valueType: 'option',
            render: (_, record) => [
                <Button
                    type="link"
                    size="small"
                    key="preview"
                    hidden={!access.hasPerms('tool:gen:edit')}
                    onClick={() => {
                        previewCode(record.tableName).then((res) => {
                            if (res.code === 200) {
                                setPreivewData(res.data);
                                setShowPreview(true);
                            } else {
                                message.error('获取数据失败');
                            }
                        });
                    }}
                >
                    预览
                </Button>,
                <Button
                    type="link"
                    size="small"
                    key="edit"
                    hidden={!access.hasPerms('tool:gen:edit')}
                    onClick={() => {
                        // setEditModalVisible(true);
                        // setCurrentRow(record);
                        getBjgenInfo(record.tableName).then((res) => {
                            setEditModalVisible(true);
                            setCurrentRow(record);
                            if (res.code === 200) {
                                if(res.hasOwnProperty("data")){
                                    setBjgenInfo(res.data);
                                }else{
                                    setBjgenInfo({});
                                }
                                
                            } else {
                                message.error('获取数据失败');
                            }
                        });
                    }}
                >
                    编辑
                </Button>,
            ],
        },
    ];

    return (
        <Content>
            <Card>
                <ProTable<BjgenTableType>
                    headerTitle="代码生成信息"
                    actionRef={actionRef}
                    formRef={formTableRef}
                    rowKey="tableName"
                    request={(params) =>
                        getBjgenTableList({ ...params } as BjgenTableListParams).then((res) => {
                            console.log({ res });
                            return {
                                data: res.rows,
                                total: res.total,
                                success: true,
                            };
                        })
                    }
                    columns={columns}
                />
                <PreviewForm
                    open={showPreview}
                    data={preivewData}
                    onHide={() => {
                        setShowPreview(false);
                    }}
                />
                <EditForm
                    onSubmit={async (values) => {
                        let success = false;
                        console.log(bjgenInfo);
                        
                        if (Object.keys(bjgenInfo).length !== 0) {
                            success = await handleUpdate({ ...values } as BjgenTableType);
                        } else {
                            success = await handleAdd({ ...values } as BjgenTableType);
                        }

                        if (success) {
                            setEditModalVisible(false);
                            setCurrentRow(undefined);
                            if (actionRef.current) {
                                actionRef.current.reload();
                            }
                        }
                    }}
                    onCancel={() => {
                        setEditModalVisible(false);
                        setCurrentRow(undefined);
                    }}
                    open={editModalVisible}
                    values={currentRow || {}}
                    data={bjgenInfo}

                />
            </Card>
        </Content>
    );
};

export default BjgenCodeView;