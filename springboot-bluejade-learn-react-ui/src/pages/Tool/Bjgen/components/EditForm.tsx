
import { ProForm, ProFormText } from "@ant-design/pro-components";
import { FormattedMessage } from "@umijs/max";
import { Form, Modal } from "antd";
import { useEffect } from "react";
import { BjgenTableType } from "../data";

export type BjgenFormData = Record<string, unknown> & Partial<BjgenTableType>;

export type BjgenFormProps = {
    onCancel: (flag?: boolean, formVals?: BjgenFormData) => void;
    onSubmit: (values: BjgenFormData) => Promise<void>;
    open: boolean;
    values: Partial<BjgenTableType>;
    data?: any;
};

const EditForm: React.FC<BjgenFormProps> = (props) => {
    const [form] = Form.useForm();

    useEffect(() => {
        form.resetFields();
        form.setFieldsValue({
            tableId: props.data.tableId,
            tableName: props.values.tableName,
            tableComment: props.values.tableComment,
            packageName: props.data.packageName,
            moduleName: props.data.moduleName,
            businessName: props.data.businessName,
            functionName: props.data.functionName,
        });
    }, [form, props]);

    const handleOk = () => {
        form.submit();
    };
    const handleCancel = () => {
        props.onCancel();
    };
    const handleFinish = async (values: Record<string, any>) => {
        props.onSubmit(values as BjgenFormData);
    };

    return (
        <Modal
            width={640}
            title={"编辑"}
            forceRender
            open={props.open}
            destroyOnClose
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <ProForm
                form={form}
                grid={true}
                submitter={false}
                layout="horizontal"
                onFinish={handleFinish}>
                <ProFormText
                    name="tableName"
                    label={"表名"}
                    disabled
                />
                <ProFormText
                    name="tableComment"
                    label={"表描述"}
                    disabled
                />
                <ProFormText
                    name="packageName"
                    label={"生成包路径"}
                    placeholder="请输入"
                    rules={[
                        {
                            required: true,
                            message: <FormattedMessage id="请输入" defaultMessage="请输入" />,
                        },
                    ]}
                />
                <ProFormText
                    name="moduleName"
                    label={"模块名称"}
                    placeholder="请输入"
                    rules={[
                        {
                            required: true,
                            message: <FormattedMessage id="请输入" defaultMessage="请输入" />,
                        },
                    ]}
                />
                <ProFormText
                    name="businessName"
                    label={"业务名称"}
                    placeholder="请输入"
                    rules={[
                        {
                            required: true,
                            message: <FormattedMessage id="请输入" defaultMessage="请输入" />,
                        },
                    ]}
                />
                <ProFormText
                    name="functionName"
                    label={"功能名称"}
                    placeholder="请输入"
                    rules={[
                        {
                            required: true,
                            message: <FormattedMessage id="请输入" defaultMessage="请输入" />,
                        },
                    ]}
                />
            </ProForm>

        </Modal>

    );
};
export default EditForm;