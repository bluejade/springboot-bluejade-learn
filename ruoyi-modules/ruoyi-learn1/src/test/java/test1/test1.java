package test1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class test1 {
    public static void main(String[] args) {
        ArrayListTest1();

        HashMapTest1();

    }

    private static void HashMapTest1() {
        HashMap<String,Integer> hm = new HashMap<>();
        hm.put("1号",100);
        hm.put("2号",900);
        hm.put("3号",7000);

        for (HashMap.Entry<String,Integer> h:hm.entrySet()
             ) {
            System.out.println(h);
        }
        System.out.println("HashMapTest1");
    }

    private static void ArrayListTest1() {
        /**
         * ArrayList
         */
        ArrayList<String> strings = new ArrayList<>();
        strings.add("123");
        strings.add("wqb");
        strings.add("啥都");

        String s = "胡子";
        for (String string:strings
             ) {
            String s2 = string + " - " + s ;
            System.out.println(s2);
        }
        System.out.println("ArrayList Test");
    }



}
