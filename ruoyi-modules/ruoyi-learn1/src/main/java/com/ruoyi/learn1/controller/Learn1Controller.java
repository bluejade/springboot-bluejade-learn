package com.ruoyi.learn1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/learn1")
public class Learn1Controller {
    @GetMapping("/test1")
    public String test1()
    {
        System.out.println("1234");
        return "1234";
    }
}
