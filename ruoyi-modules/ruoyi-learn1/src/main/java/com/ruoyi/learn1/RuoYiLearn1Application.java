package com.ruoyi.learn1;

import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class RuoYiLearn1Application {
    public static void main(String[] args) {
        SpringApplication.run(RuoYiLearn1Application.class, args);
        System.out.println("开始学习sb");
    }
}
