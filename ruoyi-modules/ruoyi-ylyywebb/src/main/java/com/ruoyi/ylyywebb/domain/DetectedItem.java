package com.ruoyi.ylyywebb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.web.domain.BaseEntity;

public class DetectedItem extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 检测项目ID */
    private Long detectedItemId;
    /** 检测项目名称 */
    private String detectedItemName;

    public  Long getDetectedItemId() {
        return detectedItemId;
    }

    public  void setDetectedItemId(Long detectedItemId){
        this.detectedItemId = detectedItemId;
    }

    public  String getDetectedItemName() {
        return detectedItemName;
    }

    public  void setDetectedItemName(String detectedItemName){
        this.detectedItemName = detectedItemName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("detectedItemId",getDetectedItemId())
                .append("detectedItemName",getDetectedItemName())
                .toString();
    }
}
