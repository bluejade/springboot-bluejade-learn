package com.ruoyi.ylyywebb.mapper;

import java.util.List;
import com.ruoyi.ylyywebb.domain.InstrumentLocation;

public interface InstrumentLocationMapper{

    public List<InstrumentLocation> selectInstrumentLocationList(InstrumentLocation instrumentLocation);

    public InstrumentLocation selectInstrumentLocationById(Long instrumentLocation);

    public int insertInstrumentLocation(InstrumentLocation instrumentLocation);

    public int updateInstrumentLocation(InstrumentLocation instrumentLocation);

    public int deleteInstrumentLocationByIds(Long[] ids);
}
