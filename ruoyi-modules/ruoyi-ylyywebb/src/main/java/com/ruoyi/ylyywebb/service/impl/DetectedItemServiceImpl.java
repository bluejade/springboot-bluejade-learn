package com.ruoyi.ylyywebb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ylyywebb.mapper.DetectedItemMapper;
import com.ruoyi.ylyywebb.domain.DetectedItem;
import com.ruoyi.ylyywebb.service.IDetectedItemService;

@Service
public class DetectedItemServiceImpl implements IDetectedItemService{

    @Autowired
    private DetectedItemMapper detectedItemMapper;

    @Override
    public List<DetectedItem> selectDetectedItemList(DetectedItem detectedItem) {
        return detectedItemMapper.selectDetectedItemList(detectedItem);
    }

    @Override
    public DetectedItem selectDetectedItemById(Long id){
        return detectedItemMapper.selectDetectedItemById(id);
    }

    @Override
    public int insertDetectedItem(DetectedItem detectedItem){
        return detectedItemMapper.insertDetectedItem(detectedItem);
    }

    @Override
    public int updateDetectedItem(DetectedItem detectedItem){
        return detectedItemMapper.updateDetectedItem(detectedItem);
    }

    @Override
    public int deleteDetectedItemByIds(Long[] ids) {
        return detectedItemMapper.deleteDetectedItemByIds(ids);
    }
}
