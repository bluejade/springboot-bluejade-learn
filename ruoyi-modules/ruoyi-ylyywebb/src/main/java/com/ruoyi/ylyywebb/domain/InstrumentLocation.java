package com.ruoyi.ylyywebb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.web.domain.BaseEntity;

public class InstrumentLocation extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 仪器位置ID */
    private Long instrumentLocationId;
    /** 位置名称 */
    private String locationName;
    /** 经度 */
    private float longitude;
    /** 纬度 */
    private float latitude;

    public  Long getInstrumentLocationId() {
        return instrumentLocationId;
    }

    public  void setInstrumentLocationId(Long instrumentLocationId){
        this.instrumentLocationId = instrumentLocationId;
    }

    public  String getLocationName() {
        return locationName;
    }

    public  void setLocationName(String locationName){
        this.locationName = locationName;
    }

    public  float getLongitude() {
        return longitude;
    }

    public  void setLongitude(float longitude){
        this.longitude = longitude;
    }

    public  float getLatitude() {
        return latitude;
    }

    public  void setLatitude(float latitude){
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("instrumentLocationId",getInstrumentLocationId())
                .append("locationName",getLocationName())
                .append("longitude",getLongitude())
                .append("latitude",getLatitude())
                .toString();
    }
}
