package com.ruoyi.ylyywebb.service;

import java.util.List;
import com.ruoyi.ylyywebb.domain.DetectedItem;

public interface IDetectedItemService{

    public List<DetectedItem> selectDetectedItemList(DetectedItem detectedItem);

    public DetectedItem selectDetectedItemById(Long id);

    public int insertDetectedItem(DetectedItem detectedItem);

    public int updateDetectedItem(DetectedItem detectedItem);

    public int deleteDetectedItemByIds(Long[] ids);
}
