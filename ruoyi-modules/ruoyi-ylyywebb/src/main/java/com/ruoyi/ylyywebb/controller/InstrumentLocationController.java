package com.ruoyi.ylyywebb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;

import com.ruoyi.ylyywebb.domain.InstrumentLocation;
import com.ruoyi.ylyywebb.service.IInstrumentLocationService;

@RestController
@RequestMapping("/instrumentLocation")
public class InstrumentLocationController extends BaseController{

    @Autowired
    private IInstrumentLocationService instrumentLocationService;

    @GetMapping("/list")
    public TableDataInfo list(InstrumentLocation instrumentLocation)
    {
        startPage();
        List<InstrumentLocation> list = instrumentLocationService.selectInstrumentLocationList(instrumentLocation);
        return getDataTable(list);
    }

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(instrumentLocationService.selectInstrumentLocationById(id));
    }

    @PostMapping
    public AjaxResult add(@RequestBody InstrumentLocation instrumentLocation)
    {
        return toAjax(instrumentLocationService.insertInstrumentLocation(instrumentLocation));
    }

    @PutMapping
    public AjaxResult edit(@RequestBody InstrumentLocation instrumentLocation)
    {
        return toAjax(instrumentLocationService.updateInstrumentLocation(instrumentLocation));
    }

    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(instrumentLocationService.deleteInstrumentLocationByIds(ids));
    }
}
