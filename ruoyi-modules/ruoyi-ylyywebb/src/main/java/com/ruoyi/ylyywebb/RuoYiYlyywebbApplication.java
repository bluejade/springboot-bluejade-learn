package com.ruoyi.ylyywebb;

import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class RuoYiYlyywebbApplication {
    public static void main(String[] args) {
        SpringApplication.run(RuoYiYlyywebbApplication.class,args);
        System.out.println("易联云仪web端模块启动！");
    }
}
