package com.ruoyi.ylyywebb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ylyywebb.mapper.InstrumentLocationMapper;
import com.ruoyi.ylyywebb.domain.InstrumentLocation;
import com.ruoyi.ylyywebb.service.IInstrumentLocationService;

@Service
public class InstrumentLocationServiceImpl implements IInstrumentLocationService{

    @Autowired
    private InstrumentLocationMapper instrumentLocationMapper;

    @Override
    public List<InstrumentLocation> selectInstrumentLocationList(InstrumentLocation instrumentLocation) {
        return instrumentLocationMapper.selectInstrumentLocationList(instrumentLocation);
    }

    @Override
    public InstrumentLocation selectInstrumentLocationById(Long id){
        return instrumentLocationMapper.selectInstrumentLocationById(id);
    }

    @Override
    public int insertInstrumentLocation(InstrumentLocation instrumentLocation){
        return instrumentLocationMapper.insertInstrumentLocation(instrumentLocation);
    }

    @Override
    public int updateInstrumentLocation(InstrumentLocation instrumentLocation){
        return instrumentLocationMapper.updateInstrumentLocation(instrumentLocation);
    }

    @Override
    public int deleteInstrumentLocationByIds(Long[] ids) {
        return instrumentLocationMapper.deleteInstrumentLocationByIds(ids);
    }
}
