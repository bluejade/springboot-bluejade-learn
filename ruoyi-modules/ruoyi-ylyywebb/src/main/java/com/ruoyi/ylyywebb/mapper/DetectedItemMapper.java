package com.ruoyi.ylyywebb.mapper;

import java.util.List;
import com.ruoyi.ylyywebb.domain.DetectedItem;

public interface DetectedItemMapper{

    public List<DetectedItem> selectDetectedItemList(DetectedItem detectedItem);

    public DetectedItem selectDetectedItemById(Long id);

    public int insertDetectedItem(DetectedItem detectedItem);

    public int updateDetectedItem(DetectedItem detectedItem);

    public int deleteDetectedItemByIds(Long[] ids);
}
