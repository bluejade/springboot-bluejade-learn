package com.ruoyi.ylyywebb.controller;

import java.util.List;


import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.core.web.domain.AjaxResult;

import com.ruoyi.ylyywebb.domain.DetectedItem;
import com.ruoyi.ylyywebb.service.IDetectedItemService;

@RestController
@RequestMapping("/detectedItem")
public class DetectedItemController extends BaseController {

    @Autowired
    private IDetectedItemService detectedItemService;

    @GetMapping("/list")
    public TableDataInfo list(DetectedItem detectedItem)
    {
        startPage();
        List<DetectedItem> list = detectedItemService.selectDetectedItemList(detectedItem);
        return getDataTable(list);
    }

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(detectedItemService.selectDetectedItemById(id));
    }

    @PostMapping
    public AjaxResult add(@RequestBody DetectedItem detectedItem)
    {
        return toAjax(detectedItemService.insertDetectedItem(detectedItem));
    }

    @PutMapping
    public AjaxResult edit(@RequestBody DetectedItem detectedItem)
    {
        return toAjax(detectedItemService.updateDetectedItem(detectedItem));
    }

    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(detectedItemService.deleteDetectedItemByIds(ids));
    }
}
