package com.ruoyi.excel.service;

import javax.servlet.ServletOutputStream;
import java.io.InputStream;

public interface IUserService {

    void exportUserInfo(ServletOutputStream outputStream);

    void importUserInfo(InputStream inputStream);
}
