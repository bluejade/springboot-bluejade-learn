package com.ruoyi.excel.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.ruoyi.common.core.text.StrFormatter;
import com.ruoyi.excel.domain.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONUtil;

import java.util.List;
@Slf4j

public class ExcelDataListener implements ReadListener<User> {

    /**
     * 100条
     */
    private static final int BATCH_COUNT = 10;

    // 缓存数据
    private static final List<User> CACHED_DATA_LIST = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    // 错误信息
    @Getter
    private String errorMsg;

    // 这个每一条数据解析都会来调用
    @Override
    public void invoke(User user, AnalysisContext analysisContext) {
        log.info("解析到一条数据:{}", String.valueOf(user));
        // TODO 校验导入数据是否合规
        // 如果不合规
        this.errorMsg = StrFormatter.format("导入数据第{}行校验不通过！", analysisContext.readRowHolder().getRowIndex());
        CACHED_DATA_LIST.add(user);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (CACHED_DATA_LIST.size() >= BATCH_COUNT) {
            // TODO 保存数据到MySQL
            log.info("入库开始");
            for (User user1 : CACHED_DATA_LIST){
                log.info(String.valueOf(user1));
            }
            log.info("入库完成");

            // 存储完成置空list
            CACHED_DATA_LIST.clear();
        }
    }

    // 所有数据解析完成了 都会来调用
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        // TODO 保存数据到MySQL
        log.info("所有数据解析完成！");
    }

}
