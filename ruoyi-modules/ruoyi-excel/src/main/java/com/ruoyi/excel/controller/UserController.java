package com.ruoyi.excel.controller;

import com.ruoyi.excel.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/excel")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("/export")
    public void exportUserInfo(HttpServletResponse response){
        try {
            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            String fileName = "导出用户信息信息列表";
            response.setHeader("Content-disposition",
                    "attachment;filename*=utf-8'zh_cn'" + fileName + System.currentTimeMillis() + ".xlsx");
            userService.exportUserInfo(response.getOutputStream());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @PostMapping("/import")
    public void importUserInfo(@RequestParam(value = "file")MultipartFile file){
        try {
            userService.importUserInfo(file.getInputStream());
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
