package com.ruoyi.excel.service.Impl;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.nacos.shaded.com.google.common.collect.Lists;
import com.ruoyi.excel.domain.User;
import com.ruoyi.excel.service.IUserService;
import com.ruoyi.excel.utils.ExcelDataListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements IUserService {

    @Override
    public void exportUserInfo(ServletOutputStream outputStream){
        // 1
        ExcelWriter excelWriter = EasyExcelFactory.write(outputStream).build();
        WriteSheet userSheet = EasyExcelFactory.writerSheet(0)
                .head(User.class)
//                // 导出文件不包含列名
//                .excludeColumnFieldNames(Lists.newArrayList())
                // 导出文件包含列名
                .sheetName("uSheet1")
                .includeColumnFieldNames(Lists.newArrayList("name"))
                .build();
        excelWriter.write(this::getUserList, userSheet);
        excelWriter.finish();


        // 2
//        EasyExcelFactory.write(outputStream, User.class).sheet("userInfo").doWrite(this::getUserList);
    }

    private List<User> getUserList(){
        log.info("导出");
        return Collections.singletonList(User.builder()
                .id(2L).name("bluejade2").remark("备注")
                .build());
    }

    @Override
    public void importUserInfo(InputStream inputStream) {
        // 第一种方式
//        ExcelDataListener excelDataListener = new ExcelDataListener();
//        ExcelReader excelReader = EasyExcelFactory.read(inputStream).build();
//        ReadSheet userSheet = EasyExcelFactory.readSheet(0)
//                .head(User.class)
//                .registerReadListener(excelDataListener)
//                .build();
//        excelReader.read(userSheet);
//        // 拿到错误信息，返回前端
//        String errorMsg = excelDataListener.getErrorMsg();

        // 第二种方式
        // 调用默认监听器
//        EasyExcelFactory.read(inputStream, User.class, new ReadListener<User>() {
//            /**
//             * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
//             */
//            private static final int BATCH_COUNT = 100;
//            /**
//             * 缓存的数据
//             */
//            private final List<User> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
//
//            @Override
//            public void invoke(User user, AnalysisContext analysisContext) {
//                cachedDataList.add(user);
//            }
//            @Override
//            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//                cachedDataList.forEach(user -> log.info(user.toString()));
//            }
//        }).sheet().doRead();

        // 3
        // 调用自定义的监听器 ExcelDataListener 自己实现 invoke  doAfterAllAnalysed方法
        EasyExcelFactory.read(inputStream, User.class, new ExcelDataListener()).sheet().doRead();


    }

}
