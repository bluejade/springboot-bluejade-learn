package com.ruoyi.bjgen.controller;

import com.ruoyi.bjgen.domain.BjgenTableColumn;
import com.ruoyi.bjgen.service.IBjgenTableColumnService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/bjgen/tableColumn")
public class BjgenColumnController extends BaseController {
    @Autowired
    private IBjgenTableColumnService bjgenTableColumnService;

    @GetMapping("/list")
    public TableDataInfo list(BjgenTableColumn bjgenTableColumn){
        startPage();
        List<BjgenTableColumn> list=bjgenTableColumnService.selectBjgenTableColumnList(bjgenTableColumn);
        return getDataTable(list);
    }

    @GetMapping("/ColumnList")
    public TableDataInfo columnList(BjgenTableColumn bjgenTableColumn){
        startPage();
        List<BjgenTableColumn> list = bjgenTableColumnService.selectBjgenTableColumnListByTableName(bjgenTableColumn);
        return getDataTable(list);
    }
    /**
     * 代码预览
     */
    @GetMapping("/preview/{tableName}")
    public AjaxResult preview(@PathVariable("tableName") String tableName) throws IOException {
        Map<String, String> dataMap = bjgenTableColumnService.previewCode(tableName);
        return success(dataMap);
    }
}
