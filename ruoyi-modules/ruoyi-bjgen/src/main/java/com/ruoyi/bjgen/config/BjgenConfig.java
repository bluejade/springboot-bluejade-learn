package com.ruoyi.bjgen.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author bluejade
 */
@Component
@ConfigurationProperties(prefix = "bjgen")
public class BjgenConfig {
    public static String author;

    /** 生成包路径 */
    public static  String packageName;

    /** 自动去除表前缀，默认是false */
    public static boolean autoRemovePre;

    public static String tablePrefix;

    public static String getAuthor() {
        return author;
    }

    public static void setAuthor(String author) {
        BjgenConfig.author = author;
    }

    public static String getPackageName() {
        return packageName;
    }

    public static void setPackageName(String packageName) {
        BjgenConfig.packageName = packageName;
    }

    public static boolean isAutoRemovePre() {
        return autoRemovePre;
    }

    public static void setAutoRemovePre(boolean autoRemovePre) {
        BjgenConfig.autoRemovePre = autoRemovePre;
    }

    public static String getTablePrefix() {
        return tablePrefix;
    }

    public static void setTablePrefix(String tablePrefix) {
        BjgenConfig.tablePrefix = tablePrefix;
    }

}
