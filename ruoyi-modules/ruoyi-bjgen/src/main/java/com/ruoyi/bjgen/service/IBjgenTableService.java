package com.ruoyi.bjgen.service;

import com.ruoyi.bjgen.domain.BjgenTable;

import java.util.List;
import java.util.Map;

/**
 * 业务 服务层
 *
 * @author bluejade
 */
public interface IBjgenTableService {
    /**
     * 查询业务列表
     *
     * @param bjgenTable 业务信息
     * @return 业务集合
     */
    List<BjgenTable> selectBjgenTableList(BjgenTable bjgenTable);

    public Map<String, String> previewCode(Long tableId);

    public int insertBjgenTable(BjgenTable bjgenTable);

    public int updateBjgenTable(BjgenTable bjgenTable);

    public BjgenTable selectBjgenTableByTableName(String tableName);
}
