package com.ruoyi.bjgen.controller;

import com.ruoyi.bjgen.domain.BjgenTable;
import com.ruoyi.bjgen.domain.BjgenTableColumn;
import com.ruoyi.bjgen.service.IBjgenTableService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RequestMapping("/bjgen/table")
@RestController
public class BjgenController extends BaseController {

    @Autowired
    private IBjgenTableService bjgenTableService;

    /**
     * 查询所有表列表
     */
    @GetMapping("/list")
    public TableDataInfo bjgenList(BjgenTable bjgenTable) {
        startPage();
        List<BjgenTable> list = bjgenTableService.selectBjgenTableList(bjgenTable);
        return getDataTable(list);
    }

    @GetMapping("/preview/{tableId}")
    public AjaxResult preview(@PathVariable("tableId") Long tableId) throws IOException {
        Map<String, String> dataMap = bjgenTableService.previewCode(tableId);
        return success(dataMap);
    }

    @PostMapping
    public AjaxResult add(@RequestBody BjgenTable bjgenTable){
        return success(bjgenTableService.insertBjgenTable(bjgenTable));
    }

    @PutMapping
    public AjaxResult edit(@RequestBody BjgenTable bjgenTable){
        return success(bjgenTableService.updateBjgenTable(bjgenTable));
    }

    @GetMapping(value = "/tableName/{tableName}")
    public AjaxResult getInfo(@PathVariable("tableName") String tableName)
    {
        return success(bjgenTableService.selectBjgenTableByTableName(tableName));
    }
}
