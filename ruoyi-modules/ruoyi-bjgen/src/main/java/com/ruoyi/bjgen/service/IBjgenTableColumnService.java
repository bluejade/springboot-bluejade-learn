package com.ruoyi.bjgen.service;

import com.ruoyi.bjgen.domain.BjgenTableColumn;

import java.util.List;
import java.util.Map;

public interface IBjgenTableColumnService {

    public List<BjgenTableColumn> selectBjgenTableColumnList(BjgenTableColumn bjgenTableColumn);

    public Map<String, String> previewCode(String tableName);

    public List<BjgenTableColumn> selectBjgenTableColumnListByTableName(BjgenTableColumn bjgenTableColumn);
}
