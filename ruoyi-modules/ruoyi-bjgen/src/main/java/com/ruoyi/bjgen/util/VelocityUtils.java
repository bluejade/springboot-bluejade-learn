package com.ruoyi.bjgen.util;

import com.ruoyi.bjgen.domain.BjgenTable;
import com.ruoyi.bjgen.domain.BjgenTableColumn;
import org.apache.velocity.VelocityContext;

import java.util.ArrayList;
import java.util.List;

public class VelocityUtils {
    public static VelocityContext prepareContext(BjgenTable bjgenTable) {
        VelocityContext velocityContext = new VelocityContext();

        velocityContext.put("tableName",bjgenTable.getTableName());
        velocityContext.put("tableComment",bjgenTable.getTableComment());
        velocityContext.put("table",bjgenTable);
        velocityContext.put("className",bjgenTable.getClassName());
        velocityContext.put("tplCategory",bjgenTable.getTplCategory());
        velocityContext.put("columns",bjgenTable.getColumns());

        return velocityContext;
    }

    public static List<String> getTemplateList() {
        List<String> templates = new ArrayList<>();
        templates.add("vm/java/domain.java.vm");
        templates.add("vm/java/mapper.java.vm");
        templates.add("vm/java/service.java.vm");
        templates.add("vm/java/serviceImpl.java.vm");
        templates.add("vm/java/controller.java.vm");

        templates.add("vm/xml/mapper.xml.vm");

        templates.add("vm/js/index.tsx.vm");
        templates.add("vm/js/edit.tsx.vm");
        templates.add("vm/js/service.ts.vm");
        templates.add("vm/js/data.d.ts.vm");
        return templates;
    }

    public static VelocityContext prepareContextTableColumns(BjgenTable bjgenTable, List<BjgenTableColumn> tableColumns) {
        VelocityContext velocityContext = new VelocityContext();

        velocityContext.put("bjgenTable",bjgenTable);
        velocityContext.put("columns",tableColumns);
        velocityContext.put("tableColumns","tableColumns");
        velocityContext.put("columnPk",tableColumns.get(0).getRawColumnName());
        velocityContext.put("columnPkUpperCase",underscoreToCamel(tableColumns.get(0).getRawColumnName()));
        for(BjgenTableColumn tableColumn : tableColumns){
            System.out.println("tableColumn:");
            System.out.println(tableColumn.getColumnId());

        }
        return velocityContext;
    }

    public static String underscoreToCamel(String source) {
        StringBuilder result = new StringBuilder();
        String[] words = source.split("_");
        for (int i = 0; i < words.length; i++) {
            if (i == 0) {
                result.append(words[i]);
            } else {
                result.append(words[i].substring(0, 1).toUpperCase()).append(words[i].substring(1));
            }
        }
        return result.toString();
    }
}
