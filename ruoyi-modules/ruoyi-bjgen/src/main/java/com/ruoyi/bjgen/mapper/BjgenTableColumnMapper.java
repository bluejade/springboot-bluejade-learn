package com.ruoyi.bjgen.mapper;

import com.ruoyi.bjgen.domain.BjgenTableColumn;

import java.util.List;
import java.util.Map;

public interface BjgenTableColumnMapper {
    public List<BjgenTableColumn> selectBjgenTableColumnList(BjgenTableColumn bjgenTableColumn);

    public List<BjgenTableColumn> selectBjgenTableColumnListByTableName(BjgenTableColumn bjgenTableColumn);
}
