package com.ruoyi.bjgen.domain;

import com.ruoyi.common.core.web.domain.BaseEntity;

public class BjgenTableColumn extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Integer columnId;
    private Integer tableId;
    private String tableName;
    private String rawColumnName;
    private String columnName;
    private String columnNameUpperCase;
    private String columnType;
    private String columnComment;
    private String isPrimaryKey;
    private String isRequired;

    public Integer getColumnId() {
        return columnId;
    }

    public void setColumnId(Integer columnId) {
        this.columnId = columnId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getRawColumnName() {
        return rawColumnName;
    }

    public void setRawColumnName(String rawColumnName) {
        this.rawColumnName = rawColumnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = underscoreToCamel(columnName);
    }

    public String getColumnNameUpperCase() {
        return columnNameUpperCase;
    }

    public void setColumnNameUpperCase(String columnNameUpperCase) {
        this.columnNameUpperCase = underscoreToCamel2(columnNameUpperCase);
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getIsPrimaryKey() {
        return isPrimaryKey;
    }

    public void setIsPrimaryKey(String isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
    }

    public String getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(String isRequired) {
        this.isRequired = isRequired;
    }

    public static String underscoreToCamel(String source) {
        StringBuilder result = new StringBuilder();
        String[] words = source.split("_");
        for (int i = 0; i < words.length; i++) {
            if (i == 0) {
                result.append(words[i]);
            } else {
                result.append(words[i].substring(0, 1).toUpperCase()).append(words[i].substring(1));
            }
        }
        return result.toString();
    }

    public static String underscoreToCamel2(String source) {
        StringBuilder result = new StringBuilder();
        String[] words = source.split("_");
        for (String word : words) {
            result.append(word.substring(0, 1).toUpperCase()).append(word.substring(1));
        }
        return result.toString();
    }









}
