package com.ruoyi.bjgen.mapper;


import com.ruoyi.bjgen.domain.BjgenTable;

import java.util.List;

public interface BjgenTableMapper {
    public List<BjgenTable> selectBjgenTableList(BjgenTable bjgenTable);

    public BjgenTable selectBjgenTableById(Long id);

    public int insertBjgenTable(BjgenTable bjgenTable);

    public int updateBjgenTable(BjgenTable bjgenTable);

    public BjgenTable selectBjgenTableByTableName(String tableName);
}
