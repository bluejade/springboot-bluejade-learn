package com.ruoyi.bjgen.service;

import com.ruoyi.bjgen.domain.BjgenTable;
import com.ruoyi.bjgen.domain.BjgenTableColumn;
import com.ruoyi.bjgen.mapper.BjgenTableColumnMapper;
import com.ruoyi.bjgen.mapper.BjgenTableMapper;
import com.ruoyi.bjgen.util.VelocityInitializer;
import com.ruoyi.bjgen.util.VelocityUtils;
import com.ruoyi.common.core.constant.Constants;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class BjgenTableColumnServiceImpl implements IBjgenTableColumnService{
    @Autowired
    private BjgenTableColumnMapper bjgenTableColumnMapper;

    @Autowired
    private BjgenTableMapper bjgenTableMapper;

    @Override
    public List<BjgenTableColumn> selectBjgenTableColumnList(BjgenTableColumn bjgenTableColumn) {
        return bjgenTableColumnMapper.selectBjgenTableColumnList(bjgenTableColumn);
    }

    @Override
    public Map<String, String> previewCode(String tableName) {
        Map<String, String> dataMap = new LinkedHashMap<>();
        BjgenTableColumn bjgenTableColumn = new BjgenTableColumn();
        bjgenTableColumn.setTableName(tableName);

        List<BjgenTableColumn> tableColumns = bjgenTableColumnMapper.selectBjgenTableColumnListByTableName(bjgenTableColumn);
        System.out.println(tableColumns);

        BjgenTable bjgenTable = bjgenTableMapper.selectBjgenTableByTableName(tableName);


        VelocityInitializer.initVelocity();
         VelocityContext context = VelocityUtils.prepareContextTableColumns(bjgenTable, tableColumns);

        List<String> templates = VelocityUtils.getTemplateList();
        for (String template : templates){
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);

            tpl.merge(context,sw);
            dataMap.put(template,sw.toString());
        }
        return dataMap;
    }

    @Override
    public List<BjgenTableColumn> selectBjgenTableColumnListByTableName(BjgenTableColumn bjgenTableColumn) {
        return bjgenTableColumnMapper.selectBjgenTableColumnListByTableName(bjgenTableColumn);

    }
}
