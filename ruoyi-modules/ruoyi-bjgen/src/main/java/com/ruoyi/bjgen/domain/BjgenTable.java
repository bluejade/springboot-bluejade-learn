package com.ruoyi.bjgen.domain;

import com.ruoyi.common.core.web.domain.BaseEntity;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

public class BjgenTable extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Integer tableId;
    @NotBlank(message = "表名不能为空")
    private String tableName;
    @NotBlank(message = "表描述不能为空")
    private String tableComment;
    private String className;
    private String tplCategory;

    private String packageName;
    private String moduleName;
    private String businessName;
    private String functionName;







    /** 列表信息 */
    @Valid
    private List<BjgenTableColumn> columns;

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public List<BjgenTableColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<BjgenTableColumn> columns) {
        this.columns = columns;
    }

    public String getTplCategory() {
        return tplCategory;
    }

    public void setTplCategory(String tplCategory) {
        this.tplCategory = tplCategory;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
        this.className = underscoreToCamel2(businessName);
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }



    public static String underscoreToCamel2(String source) {
        StringBuilder result = new StringBuilder();
        String[] words = source.split("_");
        for (String word : words) {
            result.append(word.substring(0, 1).toUpperCase()).append(word.substring(1));
        }
        return result.toString();
    }
}
