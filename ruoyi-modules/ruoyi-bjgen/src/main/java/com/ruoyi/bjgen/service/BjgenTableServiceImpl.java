package com.ruoyi.bjgen.service;

import com.ruoyi.bjgen.domain.BjgenTable;
import com.ruoyi.bjgen.mapper.BjgenTableMapper;
import com.ruoyi.bjgen.util.VelocityInitializer;
import com.ruoyi.bjgen.util.VelocityUtils;
import com.ruoyi.common.core.constant.Constants;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class BjgenTableServiceImpl implements IBjgenTableService{

    private static final Logger log = LoggerFactory.getLogger(BjgenTableServiceImpl.class);

    @Autowired
    private BjgenTableMapper bjgenTableMapper;

    @Override
    public List<BjgenTable> selectBjgenTableList(BjgenTable bjgenTable){
        return bjgenTableMapper.selectBjgenTableList(bjgenTable);
    }

    @Override
    public Map<String, String> previewCode(Long tableId) {
        Map<String, String> dataMap = new LinkedHashMap<>();
        BjgenTable table = bjgenTableMapper.selectBjgenTableById(tableId);
        System.out.println("table:");
        System.out.println(table);

        VelocityInitializer.initVelocity();
        VelocityContext context = VelocityUtils.prepareContext(table);
        System.out.println("context:");
        System.out.println(context);
        List<String> templates = VelocityUtils.getTemplateList();
        for (String template : templates){
            System.out.println("template:");
            System.out.println(template);

            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);

            tpl.merge(context,sw);
            dataMap.put(template,sw.toString());

            System.out.println(context);
            System.out.println(tpl);

        }
        return dataMap;
    }

    @Override
    public int insertBjgenTable(BjgenTable bjgenTable) {
        return bjgenTableMapper.insertBjgenTable(bjgenTable);
    }

    @Override
    public int updateBjgenTable(BjgenTable bjgenTable) {
        return bjgenTableMapper.updateBjgenTable(bjgenTable);
    }

    @Override
    public BjgenTable selectBjgenTableByTableName(String tableName) {
        return bjgenTableMapper.selectBjgenTableByTableName(tableName);
    }
}
